let unit = 1024;
let defaut = unit;
let size = {};
size.b = 1;
size.kb = size.b * unit;
size.mb = size.kb * unit;
size.gb = size.mb * unit;
size.tb = size.gb * unit;
size.pb = size.tb * unit;
let getUnit = (num) => {
    let sizes = Object.keys(size);
    let _unit = null;
    let _size = 0;
    function round(value, decimals) {
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    }
    sizes.forEach(unit => {
        let ct = size[unit];
        let r = num / ct;
        if (r > 0 && r < 1024 && `${r}`.split("").indexOf("e") < 0 && _unit === null) {
            _size = r;
            _unit = unit;
        }
    });
    return ({ size: round(_size, 4), unit: _unit });
};
module.exports = { getUnit };