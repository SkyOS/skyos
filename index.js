let osdir = __dirname;
let vm = require("safevm");
let fs = require("fs");
let config = (name, val = null) => {
    let path = `${osdir}/configs/${name}.json`;
    if (val !== null) {
        fs.writeFileSync(path, JSON.stringify([val]));
        return (true);
    } else {
        if (fs.existsSync(path)) {
            let data = fs.readFileSync(path).toString();
            try {
                data = JSON.parse(data)[0];
            } catch (e) {
                return (false);
            }
            return (data);
        } else {
            return ({});
        }
    }
};
let global = {};
global._fs = fs;
global.config = config;
global.vm = vm;
require(`${osdir}/vware/bios.js`)(global);